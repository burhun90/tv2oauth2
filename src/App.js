import './App.css';

import OAuth2Login from 'react-simple-oauth2-login';

const onSuccess = response => console.log(response);
const onFailure = response => console.error(response);

function App() {
  return (
    <div className="App">
      <OAuth2Login
          scope="openid"
          authorizationUrl="https://tv2norge-dev.eu.auth0.com/authorize"
          responseType="code"
          clientId="9822046hvr4lnhi7g07grihpefahy5jb"
          redirectUri="http://localhost:3000"
          onSuccess={onSuccess}
          onFailure={onFailure}
      >
          let's go
      </OAuth2Login>
    </div>
  );
}

export default App;
